
/**
 * Kudos to starbow for Quick Admin Modules; much of the current code was retooled 
 * from there for the current purpose. http://drupal.org/project/qamodules
 */

/**
 * Deactivate a module and all the modules that depend on it.
 */
Drupal.contrib_toggleDeactivate = function (title, modules) {
  modules[title].self.attr('checked', false);
  jQuery.each(modules[title].dependents, function(i, val) {
    Drupal.contrib_toggleDeactivate(val, modules);
  });
}

/**
 * Parse a module's Required by, or Depends on string.
 *
 * @param: str - ex: 'Required by: Event Views (disabled), Views UI (disabled)'
 * @return: array - ex: ['Event Views', 'Views UI']
 */
Drupal.contrib_toggleParse = function(str) {
  var result = [];
  if (str) {
    str = str.substr(str.indexOf(':')+1); // Remove 'Required by:'
    str = str.replace( /\([^)]*\)/g, ''); // Remove (stuff).
    result = jQuery.map(str.split(','), function(n,i) {
      return jQuery.trim(n);
    });
  }
  return result;
}

/**
 * Switch off all non-core modules when this module is activated.
 */
if( Drupal.jsEnabled ) {
  $(document).ready(function(){
    // Allow modules with dependencies to be modified.
    $(':checkbox:disabled').attr('disabled', false);
    // Read dependencies for all modules.
    var modules = [];
    // Repeat for each module.
	$('#system-modules input:checkbox').each( function() {
	  var $row = $(this).parents('tr:first');
	  var $title = $row.find('label:last').text();
	  var $depend = Drupal.contrib_toggleParse($row.find('.admin-dependencies').text());
	  var $required = Drupal.contrib_toggleParse($row.find('.admin-required').text());
	  modules[$title] = { self: $(this), dependencies : $depend, dependents : $required }
	});
    // Disable each non-core module, and any modules that depend on it.
    $('#system-modules input:checkbox').each( function() {
      // Identify package that module belongs to.
      var fieldsetLegend = $(this).parents('fieldset').find('legend').text();
      // Mark to be disabled all modules not in packages listed as cases.
      switch (fieldsetLegend) {
        case "Core - optional":
          break;
          
        case "Core - required":
          break;

        default:
          var title = $(this).parents('tr:first').find('label:last').text();
          Drupal.contrib_toggleDeactivate(title, modules);
      }
    });
  });
}
